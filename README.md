# fluent-bit

| Node Group | Deployment | Environments          | Namespace |
| ---------- | ---------- | --------------------- | --------- |
| all        | Argo CD    | Sandbox, Production   | logging   |

## Description

Fluent Bit is lightweight logging processor and forwarder. We use this to collect logs from applications running on Kubernetes and forward them to OpenSearch.

## Deploy

This module is deployed using Argo CD. Push changes to master, and then perform a "Sync" on the application.
